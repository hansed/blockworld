# Blocks

 A Block Voxel type world using  https://gitlab.com/hansed/scalaengine for the purpose of learning the basics of chunk mesh generation

![](https://gitlab.com/hansed/blockworld/uploads/1d1c93fa02ad7e050a2e4234e03ae75a/BlockScreenshot.png)
