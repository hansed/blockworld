package org.hansed.blocks

import org.hansed.blocks.GameEvents.SelectBlock
import org.hansed.blocks.world.World
import org.hansed.blocks.world.blocks.Blocks
import org.hansed.blocks.world.blocks.Blocks._
import org.hansed.engine.Flags.SHUTDOWN
import org.hansed.engine.core.GameScene
import org.hansed.engine.core.components.SimpleComponent
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.KeyUp
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.physics.movement.FreeKeyboardMovement
import org.hansed.engine.rendering.camera.{Camera, FreeMovingCamera, SkyboxComponent}
import org.hansed.engine.rendering.lights.PointLight
import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.Panel
import org.hansed.engine.rendering.ui.decorations.Border
import org.hansed.engine.rendering.ui.info.Label
import org.hansed.engine.{Engine, Flags}
import org.hansed.util.{Debug, GLUtil}
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
 * A Game with a world of blocks
 *
 * @author Hannes Sederholm
 */
object Game extends App {

  Engine.init(960, 540, "Blocks")

  Engine.scene = GameScene(new FreeMovingCamera(Camera.perspective(960, 540, 90.0f)))

  Engine.camera.addComponent(new SkyboxComponent("default"))

  {

    val root = Engine.scene.rootObject
    root.addChild(World)

    val lightColor = new Vector3f(1.0f, 0.8f, 0.0f)
    val lightPower = 0.3f
    val lightOffset = 0.5f

    Engine.camera.addChild(PointLight(lightColor, power = lightPower, offset = new Vector3f(0.0f, 0.5f, lightOffset)))
    Engine.camera.addChild(PointLight(lightColor, power = lightPower, offset = new Vector3f(0.0f, 0.5f, -lightOffset)))
    Engine.camera.addChild(PointLight(lightColor, power = lightPower, offset = new Vector3f(lightOffset, 0.5f, 0.0f)))
    Engine.camera.addChild(PointLight(lightColor, power = lightPower, offset = new Vector3f(-lightOffset, 0.5f, 0.0f)))

    Engine.camera.position = new Vector3f(0, 20, 0)

    Engine.camera.onComponents({
      case movement: FreeKeyboardMovement =>
        movement.middleMouseDown = true
        movement.mouseHorizontalMultiplier = 0.2f
        movement.mouseVerticalMultiplier = 0.3f
      case _ =>
    })

    GLUtil.captureCursor(true)

    root.addChild(new UI(330, 265, 300, 100) {

      val label: Label = new Label(
        s"Selected block: ${Blocks(World.selectedBlock).name}", 0, 80, 300, 20
      ) {
        hasTransparency = true
        background = UI.TRANSPARENT
        border = Border(UI.TRANSPARENT, 0)
        textAlign = UI.TEXT_ALIGN_CENTER
      }

      add(new Panel(145, 0, 10, 10) {
        background = UI.TRANSPARENT
        hasTransparency = true
      })

      add(label)

      /**
       * Deal with an event
       *
       * @param event an event sent by an EventDispatcher
       */
      override def processEvent(event: Event): Unit = {
        super.processEvent(event)

        event match {
          case SelectBlock(blockType) =>
            label.text = s"Selected block: ${Blocks(blockType).name}"
          case _ =>
        }
      }
    })

    root.addComponent(new SimpleComponent with EventListener {
      /**
       * Deal with an event
       *
       * @param event an event sent by an EventDispatcher
       */
      override def processEvent(event: Event): Unit = {
        event match {
          case KeyUp(GLFW_KEY_ESCAPE) => Flags(SHUTDOWN) = true
          case KeyUp(GLFW_KEY_I) =>
            val pos = Engine.camera.position
            Debug.log(s"Camera position: (${pos.x},${pos.y},${pos.z})", this)
          case KeyUp(GLFW_KEY_1) => Events.signal(SelectBlock(Blocks(STONE)))
          case KeyUp(GLFW_KEY_2) => Events.signal(SelectBlock(Blocks(DIRT)))
          case KeyUp(GLFW_KEY_3) => Events.signal(SelectBlock(Blocks(GRASS)))
          case KeyUp(GLFW_KEY_4) => Events.signal(SelectBlock(Blocks(LOG)))
          case KeyUp(GLFW_KEY_5) => Events.signal(SelectBlock(Blocks(LEAVES)))
          case KeyUp(GLFW_KEY_P) => Engine.camera.onComponents({
            case movement: FreeKeyboardMovement =>
              movement.middleMouseDown = !movement.middleMouseDown
            case _ =>
          })
          case _ =>
        }
      }
    })

  }

  Engine.run()

}
