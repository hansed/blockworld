package org.hansed.blocks

import org.hansed.blocks.world.blocks.Blocks.BlockType
import org.hansed.engine.core.eventSystem.events.Event

/**
 * Contains event used by the game
 *
 * @author Hannes Sederholm
 */
object GameEvents {

  /**
   * Sent to select a block for placing
   *
   * @param blockType the type of the block
   */
  case class SelectBlock(blockType: BlockType) extends Event

}
