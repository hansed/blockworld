package org.hansed.blocks.utils

import org.hansed.blocks.world.World
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Faces
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Faces.Face
import org.joml.Vector3f

/**
 * Contains helper functions for maths done on the grid
 *
 * @author Hannes Sederholm
 */
object Math {

  /**
   * Select the nearest block along a given ray
   *
   * @param start         starting point of the ray
   * @param facing        the direction of the ray
   * @param range         the maximum distance to the block
   * @param startDistance the distance at which the ray will start from starting point
   * @param step          how much the ray steps in one iteration
   * @param backStep      how much the ray steps when backtracing for the entry face
   * @return Option[(x, y, z), faceOfEntry] or None if no blocks were found on the ray
   */
  def raySelect(start: Vector3f, facing: Vector3f, range: Float,
                startDistance: Float = 0.1f, step: Float = 0.1f, backStep: Float = 0.05f
               ): Option[((Int, Int, Int), Face)] = {

    def flooredPoint(point: Vector3f): (Int, Int, Int) = {
      (floor(point.x), floor(point.y), floor(point.z))
    }

    var scale = startDistance
    val point = new Vector3f(start).add(facing.normalize(startDistance))
    facing.normalize(step)
    while (scale < range) {
      point.add(facing)
      val (x, y, z) = flooredPoint(point)

      if (World.block(x, y, z).nonEmpty) {
        facing.normalize(-backStep)
        var face: Option[Face] = None
        while (face.isEmpty) {
          point.add(facing)
          val (px, py, pz) = flooredPoint(point)
          if (px < x) {
            face = Some(Faces.LEFT)
          } else if (px >= x + 1) {
            face = Some(Faces.RIGHT)
          } else if (py < y) {
            face = Some(Faces.BOTTOM)
          } else if (py >= y + 1) {
            face = Some(Faces.TOP)
          } else if (pz < z) {
            face = Some(Faces.FRONT)
          } else if (pz >= z + 1) {
            face = Some(Faces.BACK)
          }
        }
        return Some(((x, y, z), face.get))
      }

      scale += step
    }
    None
  }

  /**
   * Chunk coordinates for given point
   *
   * @param x coordinate
   * @param z coordinate
   * @return pair of coordinates for the chunk that contains the given point
   */
  def chunk(x: Int, z: Int): (Int, Int) = {
    (floor(x.toFloat / World.CHUNK_SIZE), floor(z.toFloat / World.CHUNK_SIZE))
  }

  /**
   * Integer floor where negative numbers round down as well
   *
   * @param v the value to floor
   * @return rounded down integer
   */
  def floor(v: Float): Int = {
    if (v.toInt == v) v.toInt
    else if (v < 0) (v - 1).toInt
    else v.toInt
  }
}
