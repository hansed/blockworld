package org.hansed.blocks.world

import org.hansed.blocks.GameEvents.SelectBlock
import org.hansed.blocks.utils.{Math, OpenSimplexNoise}
import org.hansed.blocks.world.blocks.Blocks
import org.hansed.blocks.world.blocks.Blocks._
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Faces.Face
import org.hansed.blocks.world.chunks.{ChunkLoader, ChunkMeshBuilder}
import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.components.SimpleComponent
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyUp, MouseScroll}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
 * Represents the world in which the blocks reside
 *
 * @author Hannes Sederholm
 */
object World extends GameObject {

  val CHUNK_SIZE: Int = 16
  val CHUNK_HEIGHT: Int = 32

  val NOISE = new OpenSimplexNoise(156725555509L)

  /**
   * Number of chunks visible in any axis (equal number behind and in front)
   */
  val VIEW_DISTANCE = 4

  /**
   * The block selected currently for placing
   */
  var selectedBlock: BlockType = Blocks(STONE)

  {
    addComponent(new SimpleComponent with EventListener {
      override def processEvent(event: Event): Unit = {
        event match {
          case KeyUp(GLFW_KEY_K) | KeyUp(GLFW_MOUSE_BUTTON_1) =>
            facingBlock().foreach({
              case ((x, y, z), _) => setBlock(x, y, z, None)
            })
          case KeyUp(GLFW_KEY_J) | KeyUp(GLFW_MOUSE_BUTTON_2) =>
            facingBlock().foreach({
              case ((blockX, blockY, blockZ), face) =>
                val x = blockX + ChunkMeshBuilder.FACE_NORMALS(face).x.toInt
                val y = blockY + ChunkMeshBuilder.FACE_NORMALS(face).y.toInt
                val z = blockZ + ChunkMeshBuilder.FACE_NORMALS(face).z.toInt
                setBlock(x, y, z, Some(selectedBlock))
            })
          case MouseScroll(amount) =>
            var nextBlock = selectedBlock + (if (amount < 0) -1 else 1)
            if (nextBlock < 0) {
              nextBlock += Blocks.definitions.size
            } else if (nextBlock >= Blocks.definitions.size) {
              nextBlock -= Blocks.definitions.size
            }
            Events.signal(SelectBlock(nextBlock))
          case SelectBlock(blockType) =>
            selectedBlock = blockType
          case _ =>
        }
      }
    })

    addComponent(ChunkLoader)
  }

  /**
   * Find the height of the world at given position
   *
   * @param x coordinate
   * @param z coordinate
   * @return the height in blocks at given position
   */
  def height(x: Float, z: Float): Int = synchronized {
    ((NOISE.eval(x.toDouble / CHUNK_HEIGHT, z.toDouble / CHUNK_HEIGHT) + 1.0) / 2.0 * CHUNK_HEIGHT).toInt
  }

  /**
   * Find the block that the player is looking at
   *
   * @param range the range of the ray cast for finding the block
   * @return Some((x, y, z), faceOfEntry) or None if no block is in the facing direction in range
   */
  def facingBlock(range: Float = 10.0f): Option[((Int, Int, Int), Face)] = {
    Math.raySelect(Engine.camera.position, new Vector3f(0, 0, -1).rotate(Engine.camera.rotation), range)
  }

  /**
   * Find a block in a given position in the world
   *
   * @param x    coordinate of the block
   * @param y    coordinate of the block
   * @param z    coordinate of the block
   * @param load Whether or not to load chunks that haven't been loaded
   * @return None if no block is present or Some(BlockType) if a block is found at given position
   */
  def block(x: Int, y: Int, z: Int, load: Boolean = true): Option[BlockType] = {
    val (chunkX, chunkZ) = Math.chunk(x, z)
    ChunkLoader.get(chunkX, chunkZ, load)(chunk => {
      chunk(x - chunkX * CHUNK_SIZE, y, z - chunkZ * CHUNK_SIZE)
    }).flatten
  }

  /**
   * Set a block in the world
   *
   * @param x         the x coordinate of the block
   * @param y         the y coordinate of the block
   * @param z         the z coordinate of the block
   * @param blockType the new type of the block
   */
  def setBlock(x: Int, y: Int, z: Int, blockType: Option[BlockType]): Unit = {
    val (chunkX, chunkZ) = Math.chunk(x, z)
    ChunkLoader.get(chunkX, chunkZ)(chunk => {
      chunk(x - chunkX * CHUNK_SIZE, y, z - chunkZ * CHUNK_SIZE) = blockType
    })
  }


}
