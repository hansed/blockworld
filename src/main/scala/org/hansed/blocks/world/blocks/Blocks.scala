package org.hansed.blocks.world.blocks

import java.awt.image.BufferedImage

import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Corners2D.Corner2D
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Faces.Face
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.{Corners2D, Faces, TEX_INDICES_2D}
import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.textures.Material
import org.hansed.engine.saving.MaterialDefinition
import org.hansed.util.Debug
import org.joml.Vector2f

/**
 * Contains definitions for the blocks
 *
 * @author Hannes Sederholm
 */
object Blocks extends Enumeration {

  type BlockDefinition = BlockDefVal

  type BlockType = Int

  /**
   * Block definition
   *
   * @param name     the name of the block
   * @param textures mapping of textures for all faces
   */
  protected case class BlockDefVal(name: String, textures: Map[Face, String]) extends super.Val

  import scala.language.implicitConversions

  implicit def valueToBlockDefinition(v: Value): BlockDefinition = v.asInstanceOf[BlockDefinition]

  val GRASS = BlockDefVal("Grass",
    Map(
      Faces.ALL -> "textures/grass_side.png",
      Faces.TOP -> "textures/grass.png",
      Faces.BOTTOM -> "textures/dirt.png"
    )
  )
  val DIRT = BlockDefVal("Dirt", Map(Faces.ALL -> "textures/dirt.png"))
  val STONE = BlockDefVal("Stone", Map(Faces.ALL -> "textures/stone.png"))
  val LOG = BlockDefVal("Log",
    Map(
      Faces.ALL -> "textures/wood_bark.png",
      Faces.TOP -> "textures/wood_top.png",
      Faces.BOTTOM -> "textures/wood_top.png"
    )
  )
  val LEAVES = BlockDefVal("Leaves", Map(Faces.ALL -> "textures/leaves.png"))

  val ids: Map[BlockDefinition, BlockType] = Blocks.values.toVector.zipWithIndex.map({
    case (d, i) => (valueToBlockDefinition(d), i)
  }).toMap

  val definitions: Map[BlockType, BlockDefinition] = ids.map(_.swap)

  def apply(definition: BlockDefinition): BlockType = {
    ids(definition)
  }

  def forType(blockType: BlockType): BlockDefinition = {
    definitions(blockType)
  }

  val ATLAS_SIZE = 1024

  val TEX_PADDING = 16

  /**
   * Number of textures on a single row or column
   */
  val TEX_COUNT = 4

  /**
   * Size of a single texture on the atlas
   */
  val TEX_SIZE: Int = ATLAS_SIZE / TEX_COUNT

  // Assign indices to all the textures
  lazy val textureIds: Map[String, Int] = definitions.values.toVector.flatMap(_.textures.values).distinct
    .zipWithIndex.toMap

  // Mapping of block types and faces to texture indices
  lazy val textureIndex: Map[(BlockType, Face), Int] = definitions.flatMap({
    case (blockType, definition) =>
      definition.textures.flatMap({
        case (Faces.ALL, texture) =>
          Map(
            Faces.TOP -> texture,
            Faces.BOTTOM -> texture,
            Faces.LEFT -> texture,
            Faces.RIGHT -> texture,
            Faces.FRONT -> texture,
            Faces.BACK -> texture
          ).filter({
            case (direction, _) => !definition.textures.contains(direction)
          })
        case (direction: Face, texture) => Map(direction -> texture)
      }).map({
        case (direction, texture) => ((blockType, direction), textureIds(texture))
      })
  })

  // Construct the texture atlas
  val material: Material = {
    new Material(MaterialDefinition("", None)) {
      diffuse.resource.images = Array(Debug.logTime(() => {
        val textureImage = new BufferedImage(ATLAS_SIZE, ATLAS_SIZE, BufferedImage.TYPE_INT_ARGB)
        val g = textureImage.createGraphics()

        textureIds.foreach({
          case (texture, id) =>
            val image = ResourceLoader.loadImage(texture)
            g.drawImage(
              image,
              (id % TEX_COUNT) * TEX_SIZE,
              (id / TEX_COUNT) * TEX_SIZE,
              TEX_SIZE, TEX_SIZE,
              null)
        })
        g.dispose()

        ResourceLoader.convertImageToByteBuffer(textureImage)
      }, "Build texture atlas", this))
    }
  }

  val TEX_COORDS: Map[Corner2D, Vector2f] =
    Corners2D.values.toVector.map {
      case value@Corners2D.BOTTOM_LEFT => (value, new Vector2f(0.0f, 0.0f))
      case value@Corners2D.BOTTOM_RIGHT => (value, new Vector2f(1.0f, 0.0f))
      case value@Corners2D.TOP_LEFT => (value, new Vector2f(0.0f, 1.0f))
      case value@Corners2D.TOP_RIGHT => (value, new Vector2f(1.0f, 1.0f))
    }.toMap

  val TEX_COORDS_ARRAY: Vector[Vector2f] = TEX_INDICES_2D.map({
    case (corner, index) => (index, TEX_COORDS(corner))
  }).toVector.sortBy(_._1).map(_._2)

  // Calculate all the available texture coordinates in the atlas
  val textureCoords: Vector[Vector2f] = {
    (0 until TEX_COUNT * TEX_COUNT).flatMap(index => {
      TEX_COORDS_ARRAY.map(vec => {
        new Vector2f(
          (((index % TEX_COUNT) * TEX_SIZE + TEX_PADDING).toFloat + vec.x * (TEX_SIZE - 2 * TEX_PADDING)) / ATLAS_SIZE,
          1.0f - (((index / TEX_COUNT) * TEX_SIZE + TEX_PADDING).toFloat + vec.y * (TEX_SIZE - 2 * TEX_PADDING)) /
            ATLAS_SIZE
        )
      })
    }).toVector
  }

}
