package org.hansed.blocks.world.chunks

import org.hansed.blocks.world.World
import org.hansed.blocks.world.blocks.Blocks
import org.hansed.blocks.world.blocks.Blocks.BlockType
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Corners.Corner
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Corners2D.Corner2D
import org.hansed.blocks.world.chunks.ChunkMeshBuilder.Faces.Face
import org.hansed.engine.rendering.models.data.{Shape, Vertex}
import org.hansed.engine.rendering.models.loaders.ModelLoader
import org.joml.Vector3f

/**
 * A model loader that build the chunk mesh based on its block structure
 *
 * @note Use the build method for generating the shape
 * @author Hannes Sederholm
 */
object ChunkMeshBuilder extends ModelLoader {

  var chunk: ChunkResource = _

  /**
   * Generate a shape based on a chunk resource
   *
   * @param chunk the chunk resource
   * @return
   */
  def build(chunk: ChunkResource): Shape = synchronized {
    this.chunk = chunk
    loadModel("")
  }

  def addVertex(x: Int, y: Int, z: Int, corner: Corner, face: Face, blockType: BlockType): Unit = {
    inIndices += vertices.size
    inIndices += Blocks.textureIndex(blockType, face) * 4 + TEX_INDICES(corner, face)
    inIndices += FACE_INDICES(face)
    val (xOffset, yOffset, zOffset) = CORNER_OFFSETS(corner)
    vertices += Vertex(vertices.size, new Vector3f(x + xOffset, y + yOffset, z + zOffset))
  }

  def addVertices(x: Int, y: Int, z: Int, face: Face, blockType: BlockType, corners: Corner*): Unit = {
    corners.foreach(c => {
      addVertex(x, y, z, c, face, blockType)
    })
  }

  /**
   * Fill the data lists with data from chunk given in the build method
   *
   * @param path not used
   */
  override protected def readData(path: String): Unit = {

    textures ++= Blocks.textureCoords
    normals ++= FACE_NORMALS_ARRAY

    for {
      x <- 0 until World.CHUNK_SIZE
      y <- 0 until World.CHUNK_HEIGHT
      z <- 0 until World.CHUNK_SIZE
    } {
      if (chunk(x, y, z).nonEmpty) {
        Faces.values.filter(_ != Faces.ALL).foreach(face => {
          val (xOffset, yOffset, zOffset) = FACE_OFFSETS(face)
          if (y + yOffset >= World.CHUNK_HEIGHT || y + yOffset < 0 ||
            World.block(
              chunk.x * World.CHUNK_SIZE + x + xOffset,
              y + yOffset,
              chunk.z * World.CHUNK_SIZE + z + zOffset, load = false
            ).isEmpty
          ) {
            val (c1, c2, c3, c4) = CORNERS(face)
            chunk(x, y, z).foreach(addVertices(x, y, z, face, _, c1, c2, c3, c3, c4, c1))

          }
        })
      }
    }
  }

  object Corners extends Enumeration {
    type Corner = Value

    val RIGHT_TOP_BACK, RIGHT_TOP_FRONT, LEFT_TOP_BACK, LEFT_TOP_FRONT,
    RIGHT_BOTTOM_BACK, RIGHT_BOTTOM_FRONT, LEFT_BOTTOM_BACK, LEFT_BOTTOM_FRONT = Value

  }

  object Corners2D extends Enumeration {
    type Corner2D = Value

    val TOP_RIGHT, TOP_LEFT, BOTTOM_LEFT, BOTTOM_RIGHT = Value
  }

  object Faces extends Enumeration {
    type Face = Value

    val FRONT, BACK, LEFT, RIGHT, TOP, BOTTOM, ALL = Value

  }

  val FACE_OFFSETS: Map[Face, (Int, Int, Int)] =
    Faces.values.filter(_ != Faces.ALL).toVector.map({
      case value@Faces.FRONT => (value, (0, 0, -1))
      case value@Faces.BACK => (value, (0, 0, 1))
      case value@Faces.LEFT => (value, (-1, 0, 0))
      case value@Faces.RIGHT => (value, (1, 0, 0))
      case value@Faces.TOP => (value, (0, 1, 0))
      case value@Faces.BOTTOM => (value, (0, -1, 0))
    }).toMap

  val FACE_NORMALS: Map[Face, Vector3f] = FACE_OFFSETS.map({
    case (face, (x, y, z)) => (face, new Vector3f(x, y, z))
  })

  val FACE_INDICES: Map[Face, Int] = Faces.values.filter(_ != Faces.ALL).toVector.zipWithIndex.toMap

  val FACE_NORMALS_ARRAY: Vector[Vector3f] = FACE_INDICES.map({
    case (face, index) => (index, FACE_NORMALS(face))
  }).toVector.sortBy(_._1).map(_._2)

  val CORNERS: Map[Face, (Corner, Corner, Corner, Corner)] =
    Faces.values.filter(_ != Faces.ALL).toVector.map({
      case value@Faces.FRONT => (value, (
        Corners.RIGHT_BOTTOM_FRONT,
        Corners.LEFT_BOTTOM_FRONT,
        Corners.LEFT_TOP_FRONT,
        Corners.RIGHT_TOP_FRONT
      ))
      case value@Faces.BACK => (value, (
        Corners.LEFT_BOTTOM_BACK,
        Corners.RIGHT_BOTTOM_BACK,
        Corners.RIGHT_TOP_BACK,
        Corners.LEFT_TOP_BACK
      ))
      case value@Faces.TOP => (value, (
        Corners.LEFT_TOP_BACK,
        Corners.RIGHT_TOP_BACK,
        Corners.RIGHT_TOP_FRONT,
        Corners.LEFT_TOP_FRONT
      ))
      case value@Faces.BOTTOM => (value, (
        Corners.LEFT_BOTTOM_FRONT,
        Corners.RIGHT_BOTTOM_FRONT,
        Corners.RIGHT_BOTTOM_BACK,
        Corners.LEFT_BOTTOM_BACK
      ))
      case value@Faces.LEFT => (value, (
        Corners.LEFT_TOP_BACK,
        Corners.LEFT_TOP_FRONT,
        Corners.LEFT_BOTTOM_FRONT,
        Corners.LEFT_BOTTOM_BACK
      ))
      case value@Faces.RIGHT => (value, (
        Corners.RIGHT_BOTTOM_BACK,
        Corners.RIGHT_BOTTOM_FRONT,
        Corners.RIGHT_TOP_FRONT,
        Corners.RIGHT_TOP_BACK
      ))
    }).toMap

  val CORNER_OFFSETS: Map[Corner, (Int, Int, Int)] =
    Corners.values.toVector.map({
      case value@Corners.LEFT_BOTTOM_FRONT => (value, (0, 0, 0))
      case value@Corners.LEFT_BOTTOM_BACK => (value, (0, 0, 1))
      case value@Corners.LEFT_TOP_FRONT => (value, (0, 1, 0))
      case value@Corners.LEFT_TOP_BACK => (value, (0, 1, 1))
      case value@Corners.RIGHT_BOTTOM_FRONT => (value, (1, 0, 0))
      case value@Corners.RIGHT_BOTTOM_BACK => (value, (1, 0, 1))
      case value@Corners.RIGHT_TOP_FRONT => (value, (1, 1, 0))
      case value@Corners.RIGHT_TOP_BACK => (value, (1, 1, 1))
    }).toMap

  val TEX_INDICES_2D: Map[Corner2D, Int] = Corners2D.values.toVector.zipWithIndex.toMap

  val TEX_INDICES: Map[(Corner, Face), Int] =
    Corners.values.toVector.flatMap(v => {
      Faces.values.filter(_ != Faces.ALL).toVector.map((v, _))
    }).map({
      case (corner, face) =>
        val (c1, c2, c3, c4) = CORNERS(face)
        ((corner, face), if (corner == c1) {
          TEX_INDICES_2D(if (face == Faces.LEFT) Corners2D.BOTTOM_LEFT else Corners2D.TOP_LEFT)
        } else if (corner == c2) {
          TEX_INDICES_2D(if (face == Faces.LEFT) Corners2D.BOTTOM_RIGHT else Corners2D.TOP_RIGHT)
        } else if (corner == c3) {
          TEX_INDICES_2D(if (face == Faces.LEFT) Corners2D.TOP_RIGHT else Corners2D.BOTTOM_RIGHT)
        } else if (corner == c4) {
          TEX_INDICES_2D(if (face == Faces.LEFT) Corners2D.TOP_LEFT else Corners2D.BOTTOM_LEFT)
        } else {
          -1
        })
    }).toMap

}
