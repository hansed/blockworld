package org.hansed.blocks.world.chunks

import org.hansed.blocks.world.World
import org.hansed.blocks.world.blocks.Blocks.BlockType
import org.hansed.engine.Flags
import org.hansed.engine.Flags.SHUTDOWN
import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.core.resources.ResourceManager.ResourceTypeName
import org.hansed.engine.rendering.models.data.Shape
import org.hansed.engine.rendering.resources.ModelResource
import org.hansed.util.Debug

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
 * A model resource that contains the shape of a given chunk
 *
 * @param x the x coordinate of the chunk
 * @param z the z coordinate of the chunk
 * @author Hannes Sederholm
 */
class ChunkResource(val x: Int, val z: Int) extends ModelResource(s"chunk:$x:$z") {

  lazy val blocks: Array[Option[BlockType]] = Array.fill(World.CHUNK_SIZE * World.CHUNK_SIZE * World.CHUNK_HEIGHT)(None)

  var loaded = false

  /**
   * A method for loading the shape to allow overriding for alternate loading methods
   *
   * @return the shape stored in this resource
   */
  override protected def loadShape: Shape = {
    ChunkMeshBuilder.build(this)
  }

  /**
   * Recalculate the shape and request reload of the VAO once done
   *
   */
  def recalculate(): Unit = {
    Future {
      shape = loadShape
      loadCompleted = false
    }
  }

  def apply(x: Int, y: Int, z: Int): Option[BlockType] =
    blocks((z * World.CHUNK_SIZE * World.CHUNK_HEIGHT) + (y * World.CHUNK_SIZE) + x)

  /**
   * Free the resources reserved for this resource
   */
  override def clearResource(): Unit = {
    val future = Future(
      Debug.logTime(() => {
        ResourceLoader.useObjectOutputStream(s"chunks/chunk:$x:$z") {
          _.writeObject(blocks)
        }
      }, "Saving Chunk", this)
    )

    if (Flags(SHUTDOWN)) {
      Await.ready(future, Duration.Inf)
    }
    super.clearResource()
  }

  /**
   * Returns the TypeName of this resource
   *
   * Override because ResourceUser uses the subclass
   */
  override def typeName: ResourceTypeName = "org.hansed.engine.rendering.resources.ModelResource"
}
