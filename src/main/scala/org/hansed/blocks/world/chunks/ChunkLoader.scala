package org.hansed.blocks.world.chunks

import org.hansed.blocks.utils.Math
import org.hansed.blocks.world.World
import org.hansed.engine.Engine
import org.hansed.engine.core.components.Timer

import scala.collection.mutable

/**
 * Deals with loading chunks
 *
 * @author Hannes Sederholm
 */
object ChunkLoader extends Timer(1.0f) {

  var lastChunkX: Int = Integer.MAX_VALUE
  var lastChunkZ: Int = Integer.MAX_VALUE

  /**
   * Currently loaded chunks
   */
  val chunks: mutable.Map[(Int, Int), Chunk] = mutable.HashMap()

  /**
   * Get a chunk and perform an action on it
   *
   * @param chunkX chunk x
   * @param chunkZ chunk z
   * @param load   whether or not to load a chunk if it's missing
   * @param f      the callback for the chunk
   * @tparam T return type
   * @return Option[T] None if chunk was missing
   */
  def get[T](chunkX: Int, chunkZ: Int, load: Boolean = true)(f: Chunk => T): Option[T] = {
    if (chunks.contains(chunkX, chunkZ)) {
      Some(f(chunks(chunkX, chunkZ)))
    } else {
      if (load) {
        Some(f(new Chunk(chunkX, chunkZ)))
      } else {
        None
      }
    }
  }

  /**
   * Called when the timer ticks
   */
  override def tick(): Unit = {
    val (chunkX, chunkZ) = Math.chunk(Engine.camera.position.x.toInt, Engine.camera.position.z.toInt)

    if (chunkX == lastChunkX && chunkZ == lastChunkZ) return

    lastChunkX = chunkX
    lastChunkZ = chunkZ

    var xOffset = 0
    var zOffset = 0

    var stepX = 0
    var stepZ = 1

    var round = 0

    while (round < World.VIEW_DISTANCE) {

      val x = chunkX + xOffset
      val z = chunkZ + zOffset

      if (!chunks.contains((x, z))) {
        get(x, z) {
          chunk =>
            chunks((x, z)) = chunk
            World.addChild(chunk)
        }
      }

      if (xOffset == zOffset && stepZ == 1) {
        round += 1
        stepX = 0
        stepZ = 1
      } else if (zOffset == round && stepZ == 1) {
        stepX = -1
        stepZ = 0
      } else if (xOffset == -round && stepX == -1) {
        stepX = 0
        stepZ = -1
      } else if (zOffset == -round && stepZ == -1) {
        stepX = 1
        stepZ = 0
      } else if (xOffset == round && stepX == 1) {
        stepX = 0
        stepZ = 1
      }

      xOffset += stepX
      zOffset += stepZ
    }

    chunks.filterInPlace({
      case ((x, z), chunk) =>
        if (x >= chunkX - World.VIEW_DISTANCE && x <= chunkX + World.VIEW_DISTANCE &&
          z >= chunkZ - World.VIEW_DISTANCE && z <= chunkZ + World.VIEW_DISTANCE) {
          true
        } else {
          chunk.remove()
          false
        }
    })
  }
}
