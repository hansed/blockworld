package org.hansed.blocks.world.chunks

import org.hansed.blocks.world.World
import org.hansed.blocks.world.blocks.Blocks
import org.hansed.blocks.world.blocks.Blocks._
import org.hansed.engine.core.GameObject
import org.hansed.engine.core.components.Timer
import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.models.stationary.{StaticModel, StaticRenderer}
import org.hansed.engine.rendering.resources.ModelResource
import org.hansed.util.Debug
import org.joml.Vector3f

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Random

/**
 * Represents a chunk of blocks of size World.CHUNK_SIZE x World.CHUNK_SIZE x World.CHUNK_HEIGHT
 *
 * @param chunkX the x coordinate of the chunk in the grid of chunks
 * @param chunkZ the z coordinate of the chunk in the grid of chunks
 * @author Hannes Sederholm
 */
class Chunk(val chunkX: Int, val chunkZ: Int) extends GameObject {

  this.position = new Vector3f(chunkX * World.CHUNK_SIZE, 0, chunkZ * World.CHUNK_SIZE)

  val identifier: String = s"chunk:$chunkX:$chunkZ"

  val renderer = new StaticRenderer(new StaticModel(identifier) {
    override def loadResource(path: String): Option[ModelResource] = {
      Some(new ChunkResource(chunkX, chunkZ))
    }
  }, Blocks.material)

  lazy val chunkResource: ChunkResource = renderer.model.resource.asInstanceOf[ChunkResource]

  var needsRebuild = false

  addComponent(renderer)

  if (!chunkResource.loaded) {
    generate()
    chunkResource.loaded = true
  }

  /**
   * Add a timer that checks recalculation
   */
  addComponent(new Timer(0.1f) {
    override def tick(): Unit = {
      if (needsRebuild) {
        chunkResource.recalculate()
        needsRebuild = false
      }
    }
  })

  /**
   * Do the initial chunk generation
   */
  def generate(): Unit = {
    Future {
      val result = Debug.logTime(() => {
        ResourceLoader.useObjectInputStream(s"chunks/$identifier") {
          stream => stream.readObject().asInstanceOf[Array[Option[BlockType]]]
        }
      }, "Reading chunk", this)

      result.foreach(read => {
        Array.copy(read, 0, chunkResource.blocks, 0, chunkResource.blocks.length)
        needsRebuild = true
      })

      if (result.isEmpty) {
        Debug.logAction(() => {
          val rand = new Random(System.currentTimeMillis())
          for {
            x <- 0 until World.CHUNK_SIZE
            z <- 0 until World.CHUNK_SIZE
          } {

            // Default all blocks to stone
            for {
              y <- 0 until height(x, z)
            } {
              this (x, y, z) = Some(Blocks(STONE))
            }

            // Grass at top with dirt under it
            this (x, height(x, z), z) = Some(Blocks(GRASS))

            for {
              y <- height(x, z) - 1 until height(x, z) - 2 by -1
            } {
              if (y > 0) {
                this (x, y, z) = Some(Blocks(DIRT))
              }
            }

            val h = height(x, z)

            // Add some tree looking things
            if (rand.nextInt(25) == 1 && h < World.CHUNK_HEIGHT - 7 && x > 3 && x < World.CHUNK_SIZE - 3 &&
              z > 3 &&
              z < World.CHUNK_SIZE - 3) {
              for (y <- h until h + 5) {
                this (x, y, z) = Some(Blocks(LOG))
              }

              for {
                xOffset <- -1 to 1
                zOffset <- -1 to 1
              } {
                if (!(xOffset == 0 && zOffset == 0))
                  this (x + xOffset, h + 4, z + zOffset) = Some(Blocks(LEAVES))
                if (math.abs(xOffset) != math.abs(zOffset)) {
                  this (x + xOffset, h + 5, z + zOffset) = Some(Blocks(LEAVES))
                }
              }
              this (x, h + 5, z) = Some(Blocks(LEAVES))
            }

          }

        }, s"Generating chunk", this)
      }
    }
  }

  /**
   * Remove this object from its parent
   */
  override def remove(): Unit = {
    super.remove()
  }

  def height(localX: Int, localZ: Int): Int = {
    World.height(chunkX * World.CHUNK_SIZE + localX, chunkZ * World.CHUNK_SIZE + localZ)
  }

  def apply(x: Int, y: Int, z: Int): Option[BlockType] = {
    chunkResource.blocks((z * World.CHUNK_SIZE * World.CHUNK_HEIGHT) + (y * World.CHUNK_SIZE) + x)
  }

  def update(x: Int, y: Int, z: Int, blockType: Option[BlockType]): Unit = {
    chunkResource.blocks((z * World.CHUNK_SIZE * World.CHUNK_HEIGHT) + (y * World.CHUNK_SIZE) + x) = blockType
    needsRebuild = true
    for {
      xOffset <- -1 to 1
      zOffset <- -1 to 1
    } {
      if (xOffset != zOffset) {
        ChunkLoader.get(chunkX + xOffset, chunkZ + zOffset, load = false) {
          _.needsRebuild = true
        }
      }
    }
  }

}

